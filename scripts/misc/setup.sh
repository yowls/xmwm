#!/usr/bin/env bash

# Define colors
colorRed="\033[0;31m"
colorYellow="\033[0;33m"
colorGreen="\033[0;32m"
colorBlue="\033[0;34m"
colorGray="\033[0;37m"
colorEnd="\033[0m"


# Print a welcome message
function welcome(){
	echo -e $colorYellow
	echo " ______    __    ______    ______    "
	echo "/\  == \  /\ \  /\  ___\  /\  ___\   "
	echo "\ \  __<  \ \ \ \ \___  \ \ \  __\   "
	echo " \ \_\ \_\ \ \_\ \/\_____\ \ \_____\ "
	echo "  \/_/ /_/  \/_/  \/_____/  \/_____/ "
	sleep 0.6	# a dramatic cut
	echo " __  __    __    __    ______    __   __    ______    _____       "
	echo "/\_\_\_\  /\ \"-./  \\  /\  __ \  /\ \"-.\ \  /\  __ \  /\  __-.  "
	echo "\/_/\_\/_ \ \ \-./\ \ \ \ \/\ \ \ \ \-.  \ \ \  __ \ \ \ \/\ \    "
	echo "  /\_\/\_\ \ \_\ \ \_\ \ \_____\ \ \_\\\\\"\_\ \ \_\ \_\ \ \____- "
	echo "  \/_/\/_/  \/_/  \/_/  \/_____/  \/_/ \/_/  \/_/\/_/  \/____/    "
	echo -e -n $colorEnd
	sleep 0.4
}


# TODO: reference require libraries
# Check require dependencies
function checkRequire() {
	echo -e "$colorBlue   I) Checking required.. \t\t(1/5)$colorEnd"
	echo -e "\t [?] Xmonad"
	echo -e "\t [?] Xmobar"
	echo -e "\t [?] Picom"
	echo -e "\t [?] Rofi"
	echo -e "\t [?] Dunst"
	echo -e "\t [?] feh"
	echo -e "\t [?] Gnome Icon Theme"
}


# Check not so requiere but usefull dependencies
function checkOptional() {
	echo -e "$colorBlue  II) Checking optional..  \t\t(2/5)$colorEnd"
	echo -e "\t [?] $colorGray Maim $colorEnd (screenshot tool)"
	echo -e "\t [?] $colorGray Redshift $colorEnd (night light)"
	echo -e "\t [?] $colorGray Flash $colorEnd Focus (focus animations)"
	echo -e "\t [?] $colorGray EWW $colorEnd (desktop widgets)"
	echo -e "\t [?] $colorGray Xmenu $colorEnd (desktop menu)"
	echo -e "\t [?] $colorGray Xephyr $colorEnd (nested session)"
}


# Check if there is a existent xmonad config
function backup() {
	echo -e "$colorBlue III) Checking existent config.. \t(3/5)$colorEnd"

	if [ -d "$HOME/.xmonad" ]; then
		echo -e "$colorRed [WARNING] $colorEnd Detecting existent xmonad config \n"
		echo -e "+ Move the current xmonad config?"
		echo -e "\t $colorRed(~/.xmonad)$colorEnd -> $colorGreen(~/.xmonad-old)$colorEnd"

		while [[ "$backup_opt" != "y" && "$backup_opt" != "Y" && "$backup_opt" != "n" ]]
		do
			echo -e -n "\n Proceed? $colorYellow(Y/n)$colorEnd: "
			read -r backup_opt
			if [ $backup_opt = "y" ] || [ $backup_opt = "Y" ]; then
				# mv ~/.xmonad ~/.xmonad.old
				echo -e "* Done."

			elif [ $backup_opt = "n" ]; then
				echo -e "*$colorRed Directory conflict. Aborting$colorEnd"
				exit 1
			fi
		done

	else
		echo "There is no conflict between folders \n Continuing.."
	fi
}


# TODO: option to switch between any branch
# Download the repository
function implement() {
	echo -e "$colorBlue IV) Going to integrate \t\t(4/5)$colorEnd"
	echo -e "+ Download the repository with: \n\t1) HTTPS $colorGray[default]$colorEnd \n\t2) SSH"
	echo -e "\n Option: (1 or 2)"
	read -r -p "=> " clone_opt

	if [ $clone_opt -eq 2 ]; then
		echo -e "\n Cloning with$colorGreen SSH$colorEnd into ~/.xmonad .."
		# git clone git@gitlab.com:yowls/xmwm.git ~/.xmonad
	else
		echo -e "\n * Cloning with$colorGreen HTTPS$colorEnd into ~/.xmonad .."
		# git clone --depth 1 https://gitlab.com/yowls/xmwm/ ~/.xmonad
	fi
}


# TODO: check if compile works
# Compile files
function post_install() {
	echo -e "$colorBlue V) Compiling Xmonad and its config \t(5/5)$colorEnd"
	xmonad --recompile

	echo "$colorGreen[+]$colorEnd Compiling scripts and others"
	# ghc ~/.xmonad/scripts/...
	# ghc --make ~/.xmonad/scripts/xmonadctl.hs

	# If everything went correctly, print final message
	echo "All ready. \nNow log off and login using xmonad window manager"
	sleep 0.7
	echo "(if you are using a Display Manager)"
	sleep 0.5
	echo "(~laughs in xinitrc~)"
}


# -----------------------
function main() {
	welcome
	echo -e $colorYellow
	printf "%$(tput cols)s" " " | tr " " "_"
	echo -e $colorEnd

	checkRequire
	echo -e $colorYellow
	printf "%$(tput cols)s" " " | tr " " "_"
	echo -e $colorEnd

	checkOptional
	echo -e $colorYellow
	printf "%$(tput cols)s" " " | tr " " "_"
	echo -e $colorEnd

	# backup
	echo -e $colorYellow
	printf "%$(tput cols)s" " " | tr " " "_"
	echo -e $colorEnd

	# implement
	echo -e $colorYellow
	printf "%$(tput cols)s" " " | tr " " "_"
	echo -e $colorEnd

	# post_install
}
main
