--  __  __     __    __     ______     __   __     ______     _____
-- /\_\_\_\   /\ "-./  \   /\  __ \   /\ "-.\ \   /\  __ \   /\  __-.
-- \/_/\_\/_  \ \ \-./\ \  \ \ \/\ \  \ \ \-.  \  \ \  __ \  \ \ \/\ \
--   /\_\/\_\  \ \_\ \ \_\  \ \_____\  \ \_\\"\_\  \ \_\ \_\  \ \____-
--   \/_/\/_/   \/_/  \/_/   \/_____/   \/_/ \/_/   \/_/\/_/   \/____/
--     gitlab.com/yowls/xmwm

-- Haskell libraries
-- ..

-- Xmonad libraries
import XMonad
import XMonad.Layout.ShowWName
import XMonad.Util.Run (spawnPipe, hPutStrLn)
import XMonad.Util.NamedScratchpad (namedScratchpadManageHook, namedScratchpadFilterOutWorkspacePP)
import XMonad.Util.EZConfig (additionalKeysP, additionalKeys)
import XMonad.Hooks.DynamicLog

-- Custom config
import Helpers.Fundamental
import Keybinds
import Helpers.Keybinds (scratchpads)
import Rules
import Settings.Essential
import Settings.DefaultApps (term)
import ColorSchemes
import Autostart.Hooks


-- Main point
-- ==========
-- bind all the config here
--
main :: IO()
main = do
    -- Start xmobar status bar
    let xmobarDir = "~/.xmonad/StatusBars/"
    let xmobarTheme = xmobarDir ++ barTheme ++ "/" ++ barVariant ++ ".hs"
    xmproc0 <- spawnPipe $ "xmobar " ++ xmobarTheme     -- first monitor

    -- Xmobar workspaces config
    let xmobarWorkspaces = dynamicLogWithPP $ namedScratchpadFilterOutWorkspacePP xmobarPP
            { ppOutput          = hPutStrLn xmproc0
            , ppCurrent         = xmobarColor col3 ""  . wrap "{" "}"
            , ppVisible         = xmobarColor col4 ""  . wrap "" ""
            , ppHidden          = xmobarColor col2 ""  . wrap "*" ""
            , ppHiddenNoWindows = xmobarColor colFg "" . wrap "" ""
            , ppTitle           = xmobarColor col5 ""  . shorten 60
            , ppUrgent          = xmobarColor col1 ""  . wrap "!" "!"
            , ppSep             = "<fc="++ colFgi ++"> | </fc>"
            , ppWsSep           = "  "
            , ppExtras          = []
            , ppOrder           = \(ws:l:t:ex) -> [ws,l]++ex++[t] }


    -- Show workspaces name
    -- workspacePromptTheme :: SWNConfig
    let workspacePromptTheme = def
            { swn_font    = "xft:Ubuntu:bold:size=60"
            , swn_fade    = 1.0
            , swn_bgcolor = colBg
            , swn_color   = colAccent
            }

    -- Set xmonad preferences
    xmonad $ def {
      -- Basic
        terminal           = term,
        layoutHook         = showWName' workspacePromptTheme initLayout,
        workspaces         = workspaceLabels,
        borderWidth        = windowBorder,

      -- Theming
        normalBorderColor  = colBg,
        focusedBorderColor = colAccent,

      -- Rules
        focusFollowsMouse  = followMouse,
        clickJustFocuses   = additionalClick,
        manageHook         = windowRules <+> namedScratchpadManageHook scratchpads,

      -- Events and Hooks
        startupHook        = onStart,
        handleEventHook    = hooks,
        logHook            = logs <+> xmobarWorkspaces,

      -- Key bindings
        modMask            = xKey
    } `additionalKeysP` ezKeybinds
      `additionalKeys` restKeybinds
