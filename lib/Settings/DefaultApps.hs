module Settings.DefaultApps where

--  APPLICATIONS OF CHOICE
-- ========================
-- TODO: try to use environment variables first

-- Terminals
term :: [Char]    -- Default terminal
term    = "kitty"

termAlt :: [Char] -- Alternative terminal
termAlt = "urxvt" -- used for scratchpads

-- Text editors
editorGui :: [Char]
editorGui = "codium"

editorCli :: [Char]
editorCli = "nvim"

-- File managers
explorerCli :: [Char]
explorerCli = "ranger"

explorerAlt :: [Char]
explorerAlt = "pcmanfm"

-- Web Browsers
browser :: [Char]
browser     = "firefox"

browserAlt :: [Char]
browserAlt  = "brave-browser"

-- Others
chatMessages :: [Char]
chatMessages = "telegram-desktop"

musicPlayer :: [Char]
musicPlayer = "ncmpcpp"

-- Fixing command for launch
-- use default terminal for run in-terminal apps
editorCmd :: [Char]
editorCmd = term ++ " -e " ++ editorCli

explorerCmd :: [Char]
explorerCmd  = term ++ " -e " ++ explorerCli

musicPlayerCmd :: [Char]
musicPlayerCmd = termAlt ++ " -e " ++ musicPlayer


--  FILES
-- =======
-- useful file locations

-- To-do list with org mode
todo :: [Char]
todo  = "~/MEGA/parrot.org"
