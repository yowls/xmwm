module Settings.AutostartApps where

-- Autostart Applications
-- ======================
-- programs to start on login
-- < left  : application name (for check if is running)
-- > right : command to run

-- if the left side is just a ""
-- will launch on every reload

confDir :: [Char]
confDir = "~/.xmonad/lib/Settings/configs/"

applications :: [[[Char]]]
applications = [
    -- Desktop Environment Integration
    -- ["xfce-mcs-manager", "xfce-mcs-manager"]

    -- POLICYKIT | PAM | KEYRING
    ["gnome-polkit","/usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1"]
    -- ,["gnome-keyring", "gnome-keyring"]

    -- COMPOSITOR
    ,["picom", "picom -b --experimental-backends --backend glx --vsync"]
    -- , ["picom", "picom --config " ++ confDir ++ "picom_jonaburg.conf"]
    , ["flashfocus", "flashfocus -c " ++ confDir ++ "flashfocus.yml"]

    -- SYSTEM (?)
    -- ,["eww", "eww daemon"]
    ,["dunst", "dunst -conf " ++ confDir ++ "dunstrc"]
    ,["redshift", "redshift -c " ++ confDir ++ "redshift.conf"]
    -- system tray

    -- APPS
    -- ,["urxvtd",  "urxvtd -q -o -f"]
    -- ,["emacs",   "emacs --daemon"]
    -- ,["megasync","megasync"]
    -- ,["pcloud",  "pcloud"]
    -- ,["mpd",     "mpd"]
    ,["greenclip", "greenclip daemon"]

    -- LOCKSCREEN
    -- ,["xidlehook", "xidlehook --not-when-fullscreen --not-when-audio \
    --     \ --timer 300 'xbacklight -set 5' 'xbacklight -set 10' \
    --     \ --timer 590 'notify-send Suspending.. ' 'xbacklight -set 10' \
    --     \ --timer 600 'systemctl suspend' 'xbacklight -set 10' "]
    ]


-- Launch EWW windows
-- need to enable (uncomment) eww
ewws = [
    "clock"
    ,"bar"
    ]


-- Launch custom scripts at login
scripts :: [[Char]]
scripts = [
    "setxkbmap latam"
    ,"xsetroot -cursor_name left_ptr"
    ]
