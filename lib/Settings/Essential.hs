module Settings.Essential where

import XMonad

-- STATUS BAR
-- =======
-- #> Enable or disable the bar
-- values: True | False
enableBar :: Bool
enableBar = True

-- #> Random options
-- values: True | False
randomBar :: Bool
randomBar = True

-- #> Show the status bar in other monitors
-- TODO: try to get monitors with xrandr first
-- values: integer > 0
monitors :: Integer
monitors = 2

-- #> Bar theme style
-- * only works if enableBar is True
-- values:
barThemesList :: [[Char]]
barThemesList = [
    "Simpleone",        -- first theme
    ".."
    ]
barTheme :: [Char]
barTheme = "Simpleone"

-- #> Bar variant style
-- values:
--          'simple'        > one line for all the monitors
--          'composed'      > two bar for main monitor
--          'multi'         > different bars for other monitors
barVariant :: [Char]
barVariant = "simple"


-- LAYOUTS
-- =======
-- Border width of the window
-- measures in pixels
windowBorder :: Dimension
windowBorder = 3

windowMargin :: Integer
windowMargin = 6

-- Set layouts to use
-- uncomment to enable
-- NOTE: not working yet
layoutList :: [[Char]]
layoutList = [
    "tall"
    ,"magnify"
    ,"monocle"
    ,"grid"
    ,"spirals"
    ,"threeCol"
    ,"threeRow"
    ,"tabs"
    ,"tallAccordion"
    ,"wideAccordion"
    ,"floats"
    ]


-- WORKSPACES
-- ==========
-- Workspaces Labels
-- labels are what you see in the workspace widget in the bar
-- sets the number of workspaces by the length of workspaceLabels
--
workspaceLabels :: [[Char]]
-- workspaceLabels = ["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"]
workspaceLabels = ["α", "β", "γ", "δ", "ε", "ζ", "η", "θ", "ι", "κ"]
-- workspaceLabels = ["하나", "두", "세", "네", "다섯", "여섯", "일곱", "여덟", "아홉", "십"]
