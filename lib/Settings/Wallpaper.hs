module Settings.Wallpaper where

-- Application backend
-- ===================
-- #> Set the appplication to work with
-- values:
--      'feh'           > General images (jpg/png)
--      'xwinwrap'      > Animated (gif/mp4)
--      'hsetroot'      > Solid colors
method :: [Char]
method = "feh"


-- Slideshow mode
-- ==============
-- soon..


-- Random mode
-- ===========
-- #> Randomize the wallpaper
-- * for feh and xwinwrap select a random based on directory
-- * for hsetroot set a random color based on chosen palette
-- values: True | False
randomize :: Bool
randomize = False

-- TODO: allow multiples paths
-- Select directory for randomize
-- * only work if randomize = True
fehDir :: [Char]
fehDir =  "~/MEGA/Wallpaper/Photography"

xwinwrapDir :: [Char]
xwinwrapDir = "~/MEGA/Wallpaper/nimated"


-- Static
-- ======
-- TODO: integrate ~/.fehbg option
-- #> Select wallpaper by file path
-- * only work if randomize = False
fehWall :: [Char]
fehWall = "~/MEGA/Wallpaper/Neon-Cyberpunk/neotech.jpg"

xwinwrapWall ::[Char]
xwinwrapWall = "~/.xmonad/assets/wall.gif"

hsetrootWall :: [Char]
hsetrootWall = "#609060"
