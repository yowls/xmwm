module Settings.Colors where

-- COLOR SCHEME
-- ============
-- #> Choose the color scheme to use
-- integer values:
--   0         > palenight
--   1         > nihilist
--
paletteNumber :: Int
paletteNumber = 0


-- #> Random mode
-- randomize the color scheme every time
-- values: True | False
randomizeColorScheme :: Bool
randomizeColorScheme = True


-- #> Colorize applications
-- soon..
