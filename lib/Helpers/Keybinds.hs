module Helpers.Keybinds where

import XMonad.ManageHook
import XMonad.StackSet as W
import XMonad.Util.NamedScratchpad
import Settings.DefaultApps

-- Set the scratchpads
scratchpads = [
    NS  "terminal"  openTerminal    terminalTitle   terminalPosition,
    NS  "chat"      openChat        chatTitle       chatPosition,
    NS  "explorer"  openExplorer    explorerTitle   explorerPosition,
    NS  "music"     openMusic       musicTitle      musicPosition,
    NS  "emacs"     openEmacs       emacsTitle      emacsPosition,
    NS  "haskell"   openHaskell     haskellTitle    haskellPosition
    ] where
        -- Terminal options
        openTerminal    = termAlt
        terminalTitle   = title =? termAlt
        terminalPosition = customFloating $ W.RationalRect 0.02 0.05 0.96 0.45

        -- Desktop chat client
        openChat    = chatMessages
        chatTitle   = title =? "Telegram"
        chatPosition = customFloating $ W.RationalRect 0.01 0.05 0.48 0.94

        -- File explorer
        openExplorer    = termAlt ++ " -e " ++ explorerCli
        explorerTitle   = title =? explorerCli
        explorerPosition = customFloating $ W.RationalRect 0.02 0.05 0.96 0.6

        -- Music player
        openMusic   = musicPlayerCmd
        musicTitle  = title =? musicPlayer
        musicPosition = customFloating $ W.RationalRect 0.02 0.05 0.95 0.4

        -- Emacs terminal mode
        openEmacs   = termAlt ++ " -e emacs -nw " ++ todo
        emacsTitle  = title =? "emacs"
        emacsPosition = customFloating $ W.RationalRect 0.05 0.05 0.9 0.94

        -- Haskell interactive mode
        openHaskell     = termAlt ++ " -e ghci"
        haskellTitle    = title =? "ghci"
        haskellPosition = customFloating $ W.RationalRect 0.05 0.05 0.9 0.94

