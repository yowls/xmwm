module Helpers.Fundamental where

import XMonad
import XMonad.Layout.LayoutModifier
import XMonad.Hooks.ManageDocks (avoidStruts)

-- Special layouts
-- import XMonad.Layout.Spiral
-- import XMonad.Layout.ThreeColumns
-- import XMonad.Layout.Grid
-- import XMonad.Layout.MultiColumns
import XMonad.Layout.SimpleFloat

-- Space in borders
import XMonad.Layout.Spacing
-- import XMonad.Layout.Gaps
import XMonad.Layout.NoBorders


-- Define LAYOUTS
-- ==============
margin :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
margin n = spacingRaw False (Border n n n n) True (Border n n n n) True

-- TODO: floating theme
-- initLayout :: (Char) -> ()
initLayout = margin 6 $ avoidStruts currentLayouts
  where
    currentLayouts = monadTall ||| monadWide ||| max ||| floating

    -- Layouts
    monadTall   = Tall nmaster delta ratio
    monadWide   = Mirror monadTall
    max         = noBorders Full
    floating    = simpleFloat

    -- Settings
    -- The number of windows in the master pane
    nmaster = 1
    -- Percent of screen to increment by when resizing panes
    delta   = 3/100
    -- Default proportion of screen occupied by master pane
    ratio   = 1/2

