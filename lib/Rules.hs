module Rules where

import Data.Monoid
import XMonad
import Settings.Essential (workspaceLabels)

--	FOCUS POLICY
-- ================
-- Whether focus follows the mouse pointer.
followMouse :: Bool
followMouse = True

-- Whether clicking on a window to focus also passes the click to the window
additionalClick :: Bool
additionalClick = False


--	WINDOW MATCHING
-- ==========================
-- to match on the WM_NAME, you can use 'title' in the same way
-- that 'className' and 'resource' are used below.

-- #> Define what opens in X group
--
inWorkspace =
    -- Workspace 1
    [ className =? "kitty"              --> doShift (workspaceLabels !! 0)
    , className =? "Gnome-terminal"     --> doShift (workspaceLabels !! 0)

    -- Workspace 2
    , className =? "firefox"            --> doShift (workspaceLabels !! 1)
    , className =? "qutebrowser"        --> doShift (workspaceLabels !! 1)
    , className =? "brave-browser"      --> doShift (workspaceLabels !! 1)
    , className =? "Chromium-browser"   --> doShift (workspaceLabels !! 1)

    -- Workspace 3
    , className =? "VSCodium"           --> doShift (workspaceLabels !! 2)
    , className =? "Atom"               --> doShift (workspaceLabels !! 2)
    , className =? "Emacs"              --> doShift (workspaceLabels !! 2)

    -- Workspace 4
    -- , className =? "TelegramDesktop"    --> doShift (workspaceLabels !! 3)
    -- , className =? "Pcmanfm"            --> doShift (workspaceLabels !! 3)
    , className =? "Org.gnome.Nautilus" --> doShift (workspaceLabels !! 3)
    , className =? "Evince"             --> doShift (workspaceLabels !! 3)
    , className =? "Gedit"              --> doShift (workspaceLabels !! 3)
    , className =? "zathura"            --> doShift (workspaceLabels !! 3)

    -- Workspace 5
    , className =? "discord"            --> doShift (workspaceLabels !! 4)
    , className =? "zoom"               --> doShift (workspaceLabels !! 4)

    -- Workspace 6
    , className =? "Gimp-2.10"          --> doShift (workspaceLabels !! 5)
    , className =? "Libreoffice"        --> doShift (workspaceLabels !! 5) -- fix name
    , className =? "WPS"                --> doShift (workspaceLabels !! 5) -- fix name

    -- Workspace 7
    , className =? "Thunderbird"        --> doShift (workspaceLabels !! 6)

    -- Workspace 8
    , className =? "Spotify"            --> doShift (workspaceLabels !! 7)

    -- Workspace 9
    , className =? "KeePassXC"          --> doShift (workspaceLabels !! 8)
    , className =? "Bitwarden"          --> doShift (workspaceLabels !! 8)

    -- Workspace 10
    , className =? "obs"                --> doShift (workspaceLabels !! 9)
    , className =? "Cheese"             --> doShift (workspaceLabels !! 9)
    ]


-- #> Set windows in floating mode
--
floatings =
    -- System
    [ className =? "confirm"            --> doFloat
    , className =? "dialog"             --> doFloat
    , className =? "download"           --> doFloat
    , className =? "error"              --> doFloat
    , className =? "file_progress"      --> doFloat
    , className =? "notification"       --> doFloat
    , className =? "splash"             --> doFloat
    , className =? "toolbar"            --> doFloat
    , className =? "pinentry"           --> doFloat
    , className =? "pinentry-gtk-2"     --> doFloat
    , className =? "ssh-askpass"        --> doFloat
    , className =? "Xmessage"           --> doFloat

    -- Applications Titles
    , title =? "Oracle VM VirtualBox Manager"  --> doFloat

    -- Applications classname
    , className =? "Arandr"             --> doFloat
    , className =? "Xephyr"             --> doFloat
    -- , className =? "URxvt"              --> doFloat
    , className =? "XTerm"              --> doFloat
    , className =? "Dragon"             --> doFloat
    , className =? "Pcmanfm"            --> doFloat
    , className =? "Synaptic"           --> doFloat
    , className =? "Gparted"            --> doFloat
    , className =? "Pavucontrol"        --> doFloat
    , className =? "MEGAsync"           --> doFloat
    , className =? "pcloud"             --> doFloat
    , className =? "zoom"               --> doFloat
    , className =? "vlc"                --> doFloat
    , className =? "mpv"                --> doFloat
    , className =? "feh"                --> doFloat
    , className =? "ark"                --> doFloat
    , className =? "Lxappearance"       --> doFloat
    , className =? "qt5ct"              --> doFloat
    , className =? "Kvantum Manager"    --> doFloat
    , className =? "Xscreensaver-demo"  --> doFloat

    , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat
    -- , (className =? "firefox" <&&> title =? "Places") --> doFloat

    , resource  =? "desktop_window"     --> doIgnore
    , resource  =? "kdesktop"           --> doIgnore ]

-- Join the rules
windowRules :: XMonad.Query (Data.Monoid.Endo WindowSet)
windowRules = composeAll (inWorkspace ++ floatings)
