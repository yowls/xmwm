module ColorSchemes where

import Settings.Colors

-- COLOR THEMES
-- ======
-- TODO: check map dictionaries
-- list of available color palettes

themesList :: [[[Char]]]
themesList = [
    -- 0. Palenight
    [ "#292D3E",    -- 0) background
      "#ff5370",    -- 1) color1
      "#c3e88d",    -- 2) color2
      "#ffcb6b",    -- 3) color3
      "#82b1ff",    -- 4) color4
      "#c792ea",    -- 5) color5
      "#89ddff",    -- 6) color6
      "#bfc7d5",    -- 7) foreground
      "#3E4452",    -- 8) foreground inactive
      "#939ede"     -- 9) accent color
    ],

    -- 1. Nihilist
    [ "#333641",    -- 0) background
      "#ff4d4d",    -- 1) color1
      "#38ada9",    -- 2) color2
      "#f7d794",    -- 3) color3
      "#546de5",    -- 4) color4
      "#786fa6",    -- 5) color5
      "#778beb",    -- 6) color6
      "#f5f6fa",    -- 7) foreground
      "#a2a2a4",    -- 8) foreground inactive
      "#778beb"     -- 9) accent color
    ]
    ]


-- if random is enable
-- ..


-- Select one according to the number in Settings.Colors
colors = themesList!!paletteNumber

-- Split colors
col1 = colors!!1
col2 = colors!!2
col3 = colors!!3
col4 = colors!!4
col5 = colors!!5
col6 = colors!!6
colBg = colors!!0
colFg = colors!!7
colFgi = colors!!8
colAccent = colors!!9


-- Put the colors into a file for easy import
-- ..
