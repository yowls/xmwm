module Keybinds where

import System.Exit (exitSuccess)
import Control.Monad (liftM2)
import XMonad
import qualified XMonad.StackSet as W
import XMonad.Actions.CycleWS
import XMonad.Hooks.ManageDocks
import XMonad.Util.NamedScratchpad (namedScratchpadAction)
import Helpers.Keybinds
import Settings.DefaultApps
import Settings.Essential (workspaceLabels) -- todo: import monitor variable


-- SPECIAL KEYS
-- ===========
-- #> XMonad special key modMask
xKey = mod4Mask
--
-- #> Just a reminder for EZConfig mode
-- M = mod4 -> Super key
-- M1 = mod1 -> Alt key
-- S = Shift
-- C = Control


-- MOUSE BINDINGS
-- ==============
-- default actions bound to mouse events

-- NOTE: not working
-- mousebinds :: [((KeyMask, KeySym), X ())]
mousebinds =
    -- Move window in floating mode
    [ ("M-<Button1>", \w -> focus w >> mouseMoveWindow w
                                    >> windows W.shiftMaster)

    -- Raise the window
    , ("M-<Button2>", \w -> focus w >> windows W.shiftMaster)

    -- Resize window
    , ("M-<Button3>", \w -> focus w >> mouseResizeWindow w
                                    >> windows W.shiftMaster)

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]


-- KEY BINDINGS
-- ============
-- add, modify or remove key bindings here.

ezKeybinds :: [(String, X ())]
ezKeybinds =
    -- XMONAD
    -- ======
    -- #> Basics
    [ ("M-C-q",     io exitSuccess)           -- Quit
    , ("M-C-r",     spawn "xmonad --recompile; xmonad --restart")  -- Restart

    -- #> Run xmessage with a summary of the default keybindings
    -- , ("M-<F1>", spawn ("echo \"" ++ help ++ "\" | xmessage -file -"))

    -- #> Toggle the status bar gap
    -- Use with avoidStruts from Hooks.ManageDocks. See also the statusBar function from Hooks.DynamicLog.
    , ("M-y",       sendMessage ToggleStruts)

    -- #> Scratchpads
    , ("M1-|",      namedScratchpadAction scratchpads "terminal")
    , ("M-<F4>",    namedScratchpadAction scratchpads "chat")
    , ("M-<F7>",    namedScratchpadAction scratchpads "explorer")
    , ("M-<F8>",    namedScratchpadAction scratchpads "music")
    , ("M-<F9>",    namedScratchpadAction scratchpads "emacs")
    , ("M-<F10>",   namedScratchpadAction scratchpads "haskell")


    -- LAYOUTS
    -- =======
    -- #> Focusing
    , ("M-j",       windows W.focusDown)    -- Next window
    , ("M-l",       windows W.focusDown)    -- Next window
    , ("M-h",       windows W.focusUp)      -- Previous window
    , ("M-k",       windows W.focusUp)      -- Previous window
    , ("M-u",       windows W.focusMaster)  -- Master window
    , ("M1-<Tab>",  windows W.focusDown)    -- Next window
    , ("M1-S-<Tab>",windows W.focusUp)      -- Previous window
    , ("M-q",       kill)                   -- Close window

    -- #> Moving
    , ("M-C-j",     windows W.swapDown)     -- Swap with next
    , ("M-C-l",     windows W.swapDown)     -- Swap with next
    , ("M-C-h",     windows W.swapUp)       -- Swap with previous
    , ("M-C-k",     windows W.swapUp)       -- Swap with previous
    , ("M-C-u",     windows W.swapMaster)   -- Swap with master
    , ("M-i",       sendMessage (IncMasterN 1))    -- Add a Master
    , ("M-S-i",     sendMessage (IncMasterN (-1))) -- Remove a Master

    -- #> Resizing
    , ("M-S-r",     refresh)                -- Reset size
    , ("M-S-j",     sendMessage Shrink)     -- Shrink master
    , ("M-S-k",     sendMessage Expand)     -- Expand master

    -- #> Rotating layouts
    -- , ("M-S-<Space>", setLayout $ XMonad.layoutHook conf) -- Reset to default
    , ("M-<Space>", sendMessage NextLayout) -- Next layout
    -- , ("M-S-<Space>", sendMessage PreviousLayout) -- ?? Previous layout
    , ("M-s",       withFocused $ windows . W.sink) -- Toggle floating
    , ("M-m",       withFocused $ windows . W.sink) -- Toggle floating
    , ("M-f",       sendMessage ToggleStruts)


    -- SYSTEM
    -- ======
    -- #> Volume
    ,("<XF86AudioRaiseVolume>",   spawn "volume up")
    ,("<XF86AudioLowerVolume>",   spawn "volume down")
    ,("<XF86AudioMute>",          spawn "volume toggle-audio")

    -- #> Microphone
    ,("<XF86AudioMicMute>",       spawn "volume toggle-microphone")

    -- #> Music
    ,("<XF86AudioNext>",          spawn "playerctl next")
    ,("<XF86AudioPrev>",          spawn "playerctl previous")
    ,("<XF86AudioPlay>",          spawn "playerctl play-pause")

    -- #> Brightness
    ,("<XF86MonBrightnessUp>",    spawn "brightness up")
    ,("<XF86MonBrightnessDown>",  spawn "brightness down")

    -- #> Screenshots
    ,("<Print>",                  spawn "screenshot selective")   -- save in clipboard
    ,("M-<Print>",                spawn "screenshot full")        -- save full
    ,("M1-<Print>",               spawn "screenshot partial")     -- save partial
    ,("M-S-<Print>",              spawn "screenshot floating")    -- show in floating
    -- TODO: screenshot menu

    -- #> Record
    -- TODO: a record script

    -- #> Session management
    -- TODO: pending..


    -- APPLICATIONS
    -- ============
    -- #> Terminals
    , ("M-<Return>",    spawn term)
    , ("C-M1-t",        spawn termAlt)

    -- #> Text editors
    , ("M-g v",         spawn editorGui)
    , ("M-g S-v",       spawn editorCmd)

    -- #> File manager
    , ("M-g e",         spawn explorerCmd)
    , ("M-g S-e",       spawn explorerAlt)

    -- #> Browsers
    , ("M-g b",         spawn browser)
    , ("M-g S-b",       spawn browserAlt)

    -- #> Others
    , ("M-g t",         spawn "telegram-desktop")
    , ("M-g d",         spawn "discord")
    -- TODO: add more

    -- #> Emacs
    , ("M-e e",         spawn "emacs")
    , ("M-e t",         spawn ("emacs " ++ todo)) -- to do list with org
    , ("M-e c",         spawn "emacs ~/.emacs.d")
    , ("M-e x",         spawn "emacs ~/.xmonad")

    -- #> Rofi launcher
    , ("M1-<F2>",       spawn "rofi -show drun combi")
    , ("M1-<Space>",    spawn "rofi -show drun")
    , ("M1-x",          spawn "rofi -show run")
    , ("M1-w",          spawn "rofi -show window")

    , ("M-c",           spawn "rofi -modi 'clipboard:greenclip print' -show clipboard -run-command '{cmd}'")

    -- #> System Monitor
    , ("C-<Escape>",    spawn "kitty -e htop")

    -- Desktop menu
    -- , ("M1-s",          spawn "runXmenu")


    -- MULTI-MONITORS
    -- ==============
    , ("M-.",       nextScreen)             -- Switch focus to next monitor
    , ("M-,",       prevScreen)             -- Switch focus to prev monitor
    , ("M-S-,",     shiftNextScreen)        -- Move window to next monitor
    , ("M-S-,",     shiftPrevScreen)        -- Move window to prev monitor
    , ("M-C-,",     swapNextScreen)         -- Swap current screen with next monitor
    , ("M-C-,",     swapPrevScreen)         -- Swap current screen with prev monitor


    -- WORKSPACES
    -- ==========
    , ("M-<Right>",     moveTo Next noSP)       -- Focus next workspace
    , ("M-<Left>",      moveTo Prev noSP)       -- Focus previous workspace
    , ("M-<Up>",        moveTo Next NonEmptyWS) -- Focus next busy workspace
    , ("M-<Down>",      moveTo Prev NonEmptyWS) -- Focus previous busy workspace
    , ("M-S-<Right>",   shiftTo Next noSP >> moveTo Next noSP)  -- Move window to next workspace
    , ("M-S-<Left>",    shiftTo Prev noSP >> moveTo Prev noSP)  -- Move window to previous workspace
    , ("M-|",           toggleWS)               -- Switch to the last visited workspace
    ] where
        -- Avoid switching to scratchpad workspaces
        noSP = WSIs (return (\ws -> W.tag ws /= "NSP"))


-- FILLING KEYBINDS
-- ================
-- The rest of the keybinds that cant be in ezKeybinds
--
-- TODO: make workspaces dependent on the length of the workspaceLabels array
-- TODO: make switching monitors dependient of the number of monitors
restKeybinds :: [((KeyMask, KeySym), X ())]
restKeybinds =
    [ -- #> Workspaces
    -- Switch focus -> xKey + number
    -- Move window  -> xkey + Shift + number
    ((m .|. xKey, k), windows $ f i)
    | (i, k) <- zip workspaceLabels $ [xK_1 .. xK_9] ++ [xK_0]
    , (f, m) <- [(W.greedyView, 0), (liftM2 (.) W.greedyView W.shift, shiftMask)]
    ] ++
    [ -- #> Multi Monitors
    -- Switch focus -> xKey + Control + number
    -- Move window  -> xKey + Control + Shift + number
    ((m .|. xKey .|. controlMask, num), screenWorkspace sc >>= flip whenJust (windows . f))
    | (num, sc) <- zip [xK_1, xK_2] [0..]
    , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]
    ]

