module Autostart.Apps where

import System.Process
-- import System.Exit (ExitCode(ExitSuccess))
import XMonad
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import Settings.AutostartApps
-- import Helpers.Debug


-- Run functions
-- =============
-- TODO: save stderr to a file
-- TODO: if command not found throw error

-- Launch given command in shell
-- TODO: use spawnProcess instead?
run :: [Char] -> IO ProcessHandle
run name = do
    spawnCommand name

-- Check if given program name is running
-- TODO: try to use readprocess which return IO and then avoid it
running :: [[Char]] -> [Char]
running name = do
    outp <- runProcessWithInput "pgrep" ["-f"] $ head name
    if outp == "" then last name
    else ""


-- Launch programs
-- ===============
-- launchApp :: [[Char]] -> [Char]
-- launchApp name = do
--     appRunning <- running $ head name
--     if appRunning then return ""
--     else return $ last name

-- launchScript :: [Char] -> IO ProcessHandle
-- launchScript name = do
--     print "starting scripts"
--     run name

-- launchWidget :: [[Char]] -> IO ProcessHandle
-- launchWidget eWindows = do
    -- ewwRunning <- running "eww"
    -- if ewwRunning then map run ("eww open "++eWindows)
    -- else run ""
    -- print "starting eww windows"


-- Start all the weas
-- ==================
-- create an array with not-running apps
-- then run elements from that array
startApps :: X ()
startApps = do
    let notRunning = map running applications
    mapM_ spawnOnce notRunning
    --
    mapM_ spawnOnce scripts
    --
    -- mapM_ launchWidget ewws
