module Autostart.Wallpaper where

import XMonad
import XMonad.Util.SpawnOnce
import Settings.Wallpaper
-- import Palette
-- import Helpers.Debug


-- Launch wallpaper command
-- TODO: fallback mode
-- NOTE: using spawnOnce because return X()
run :: [Char] -> X ()
run wallCmd = do
    spawnOnce wallCmd


-- Wallpaper method
goFeh :: Bool -> [Char] -> [Char] -> X ()
goFeh randomize dir wall = do
    if randomize
        then run ("feh --no-fehbg --bg-fill --randomize " ++ dir)
    else run ("feh --no-fehbg --bg-fill " ++ wall)

goXwinwrap :: Bool -> [Char] -> [Char] -> X ()
goXwinwrap randomize dir wall = do
    run "notify-send not-ready"

goHsetroot :: Bool -> [[Char]] -> [Char] -> X ()
goHsetroot randomize scheme color = do
    if randomize then
        -- randomColor = ??
        run "notify-send not-ready"
    else run ("hsetroot -solid '"++color++"'")


-- Start point
goWith :: [Char] -> X ()
goWith option = do
    if option == "feh" then goFeh randomize fehDir fehWall
    else if option == "xwinwrap" then goXwinwrap randomize xwinwrapDir xwinwrapWall
    else goHsetroot randomize [""] hsetrootWall

setWallpaper :: X ()
setWallpaper = goWith method
