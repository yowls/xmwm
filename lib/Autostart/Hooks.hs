module Autostart.Hooks where

import XMonad
import XMonad.Hooks.EwmhDesktops (fullscreenEventHook, ewmhDesktopsStartup)
import XMonad.Hooks.ManageDocks (docksEventHook)
import XMonad.Hooks.SetWMName
-- import Autostart.Apps
import Autostart.Wallpaper


-- EVENT HANDLING
-- ==============
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
hooks = fullscreenEventHook <+> docksEventHook


------------------------------------------------------------------------
-- STARTUP HOOK
-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
onStart :: X()
onStart = do
    ewmhDesktopsStartup >> setWMName "Xmonad"    -- Rename to LG3D if have problems with java

    -- Set the wallpaper based on settings
    setWallpaper

    -- Start applications and scripts from settings
    -- startApps


------------------------------------------------------------------------
-- STATUS BARS AND LOGGING
-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--
logs :: X()
logs = return ()
