-- Main config for main monitor

Config {
    -- Fonts
    font = "xft:Ubuntu:weight=bold:pixelsize=12:antialias=true:hinting=true"
    , additionalFonts = []

    -- Colors
    , bgColor       = "#292D3E"
    , fgColor       = "#bfc7d5"
    , borderColor   = "#939ede"
    , border        = TopB

    -- Transparency (0-255)
    , alpha         = 255

    -- Positioning
    , position      = TopSize C 100 30
    -- , position      = Static {xpos=1366, ypos=0, width=1366, height=25}
    , textOffset    = -1

    -- Icon
    , iconRoot      = "."
    , iconOffset    = -1

    -- Extras
    , lowerOnStart  = True
    , pickBroadest  = False
    , persistent    = False
    , hideOnStart   = False
    , allDesktops   = True
    , overrideRedirect = True

    -- Elements
    , commands =
        [ Run Date "%a %b %_d %H:%M" "date" 50
        , Run Memory ["-t","Mem: <usedratio>%"] 30
        , Run Cpu ["-L","3","-H","50", "--normal","#C3E88D","--high","red"] 30
        , Run Network "wlp2s0" ["-L","0","-H","32", "--normal","green","--high","red"] 30
        , Run Com "/home/yowls/.xmonad/StatusBars/resources/updates" [] "sysUpdates" 432000
        , Run UnsafeStdinReader     -- xmonad workspaces
        ]
    , sepChar   = "%"
    , alignSep  = "}{"

    -- Sort elements to show
    , template = "\
                -- Left side
                \ %sysUpdates% | %UnsafeStdinReader% }\

                -- Center side
                \ %date% \

                -- Right side
                \{ %cpu% | %memory% | network "
    }
